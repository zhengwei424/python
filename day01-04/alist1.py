# alist = [10,35,20,80]
# print(alist)
# alist[-1] = 100
# print(alist)
# alist[1:1] = [30,50,80]
# print(alist)
# alist[1:2] = [30,50,80]
# print(alist)
# alist[1:3] = [30,50,80]
# print(alist)

# l = list()
# l.append(10)
# print(l)
# l.append([20])
# print(l)
# l.append([20])
# print(l.count([20]))


alist = [1,2,3,'bob','alice']
print(alist)
alist[0] = 10
print(alist)
alist[1:1] = [20,30]
print(alist)

alist.append(100)
print(alist)

alist.remove(10)
print(alist)
print(alist.index('bob'))
alist.insert(1,5)
print(alist)

alist.pop()
print(alist)

alist.pop(3)
print(alist)

