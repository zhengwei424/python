import  sys
import keyword
import  string

first_chr = string.ascii_letters + '_'
all_str = first_chr + string.digits

def chc_name(name):
    if keyword.iskeyword(name):
       return  'the name is keyword'
    if name[0] not in first_chr:
        return 'the 1st char is invalid'
    else:
        for ind,val in enumerate(name):
            print('%s:%s' % (ind,val))
            if name[ind] not in all_str:
                # print('\033[31;1m%s:%s\033[0m' % (ind, val))
                return '第%s个字符%s不可用' % (ind,val)

if __name__ == '__main__':

    print(chc_name(sys.argv[1]))