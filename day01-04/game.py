import random

all_choice = ['石头','剪刀','布']

computer = random.choice(all_choice)
player = input('请出拳：')

# print('computer`s choice:',computer,'your choice:',player)
print("computer`s choice: %s,your choice: %s" % (computer,player))

if player == '石头':
    if computer == '石头':
        print('go on')
    elif computer == '剪刀':
        print('you win')
    else:
        print('you lose')
elif player == '剪刀':
    if computer == '石头':
        print('you lose')
    elif computer == '剪刀':
        print('go on')
    else:
        print('you win')
else:
    if computer == '石头':
        print('you win')
    elif computer == '剪刀':
        print('you lose')
    else:
        print('go on')