import random

choices = ['石头','剪刀','布']
options = """0---石头
1---剪刀
2---布
请选择（0/1/2）："""
index = int(input(options))
player = choices[index]
computer = random.choice(choices)

print('computer`s choice: %s,your choice: %s' %(computer,player))

win_list = [['石头','剪刀'],['剪刀','布'],['布','石头']]

if [player,computer] in win_list:
    print('\033[31;1myou win\033[0m')
elif player == computer:
    print('\033[32;1mgo on\033[0m')
else:
    print('\033[31;1myou lose\033[0m')