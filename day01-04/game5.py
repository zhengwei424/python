import random

computer_win = 0
player_win = 0
all_choices = ['石头','剪刀','布']
win_list = [['石头','剪刀'],['剪刀','布'],['布','石头']]
options = """0---石头
1---剪刀
2---布
请选择(0/1/2):"""
result_list = []

while True:
    index = int(input(options))
    computer = random.choice(all_choices)
    player = all_choices[index]
    list = [player,computer]

    if player != computer:
        result_list.append(list)
        if list in win_list:
            player_win += 1
            if player_win == 2:
                print('you win')
                break
        else:
            computer_win += 1
            if computer_win == 2:
                print('computer win')
                break
print('the result is （[player,computer]）:',result_list)
