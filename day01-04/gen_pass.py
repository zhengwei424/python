from random import choice
import string

def passwd(n=8):
# all_chr = '1234567890-=qwertyuiop[]asdfghjkl;zxcvbnm,./QWERTYUIOPASDFGHJKLZXCVBNM'
    all_chr = string.ascii_letters + string.digits + string.punctuation
    result = ''
    i = 0
    for i in range(n):
        result += choice(all_chr)
    return result

if __name__ == '__main__':
    print(passwd())
    print(passwd(4))
    print(passwd(10))