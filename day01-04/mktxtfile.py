import os


def get_fname():
    while True:
        fname = input('Please enter the filepath:')
        if not os.path.exists(fname):
            break
        print('\033[32;1m"%s" \033[0m \033[31;1mis already exits , Please try again\033[0m' % fname)
    return fname


def get_content():
    content = []
    print('请输入文件内容，以"EOF"结尾')
    while True:
        s = input('>')
        if s == 'EOF':
            break
        content.append(s + '\n')
    return content


def wfile(fname, content):
    fwrite = open(fname, 'w')   #不能使用'wb'
    fwrite.writelines(content)
    fwrite.close()


if __name__ == '__main__':
    fname = get_fname()
    content = get_content()
    wfile(fname, content)
