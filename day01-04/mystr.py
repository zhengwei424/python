sentence1 = 'tom\'s pet is a cat'
sentence2 = "tom's pet is a cat"
sentence3 = "tom said:\"hello world!\""
sentence4 = 'tom said: "hello world"'
words = """hello
world
abcd"""
print(words)

py_str = 'python'
print(py_str[0])
print(py_str[1])
print(py_str[2])
print(py_str[3])
print(py_str[4])
print(py_str[5])
print(py_str[0:])   #从0开始到最后
print(py_str[1:4])  #不包含右边界

print(py_str[5:0:-1])  #!!!注意
print(py_str[5::-1])


adict = {'name': 'bob','age':23}
print(adict)
print(len(adict))
print('bob' in adict)
print('name' in adict)



