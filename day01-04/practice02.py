#创建用户名和密码，并保存在文件里面
import sys
import subprocess
from rd_passwd import passwd

def add_user(username,password,userinfo):
    data = """user information:
    %s：%s"""
    subprocess.call('useradd %s' % username ,shell=True)
    subprocess.call('echo %s | passwd --stdin %s' % (password,username),shell=True)
    with open(userinfo,'a') as fobj:
        fobj.write(data % (username,password))

if __name__ == '__main__':
    username = sys.argv[1]
    password = passwd(8)
    add_user(username,password,'/root/pss')