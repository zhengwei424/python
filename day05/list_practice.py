stack = []


def push_it():
    '''压栈'''
    item = input('item to push:')
    stack.append(item)


def pop_it():
    '''出栈'''
    if stack:
        print('from stack popped %s' % stack.pop())


def view_it():
    '''查询'''
    print(stack)


def show_menu():
    cmds = {'1': push_it, '2': pop_it, '3': view_it}
    menu = """1--压栈
2--出栈
3--查询
4--退出
请选择（1/2/3/4）："""
    while True:
        choice = input(menu).strip()[0]
        if choice not in '1234':
            print('Invalid input. Try again.')
            continue
        if choice == '4':
            break
        cmds[choice]()


if __name__ == '__main__':
    show_menu()
