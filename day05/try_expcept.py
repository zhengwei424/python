# try:
#     n = int(input('number:'))
#     result = 100/n
# except ValueError:
#     print('Invalid number')
# except ZeroDivisionError:
#     print('0 not allowed')
# except KeyboardInterrupt:
#     print('\nBye-bye')
# except EOFError:
#     print('\nBye-bye')
#
# print('Done')



try:
    n = int(input('number:'))
    result = 1/n
except(ValueError,ZeroDivisionError):
    print('Invalid number')
except(KeyboardInterrupt,EOFError):
    print('\nBye-bye')
else:
    print(result)
finally:
    print('Done')