def color(func):
    def red():
        return '\033[31;1m%s\033[0m' % func()

    return red


@color
def hello():
    return 'Hello World!'


@color
def welcome():
    return 'Hello China!'


if __name__ == '__main__':
    a = color(hello)
    b = a()
    print(b)

    print(color(hello)())

    print(hello())

    print(welcome())
