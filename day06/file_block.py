def blocks(fobj):
    block = []
    counter = 0
    for line in fobj:
        block.append(line)
        counter +=1
        if counter == 10:
            yield block
            block=[]
            counter = 0
    if block:
        yield  block


if __name__ == '__main__':
    with open('/tmp/passwd') as fobj:
        for lines in blocks(fobj):
            print(lines)
            print()
