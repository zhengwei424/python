from random import randint, choice


def add(x, y):
    return x + y


def sub(x, y):
    return x - y


def exam():
    cmds = {'+': add, '-': sub}
    num = [randint(1, 100) for i in range(2)]
    num.sort(reverse=True)
    op = choice('+-')
    result = cmds[op](*num)
    prompt = '%s %s %s =' % (num[0], op, num[1])
    if result == int(input(prompt)):
        print('you are right')
    else:
        print('you are wrong')


if __name__ == '__main__':
    while True:
        exam()
