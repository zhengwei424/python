from random import randint

def func(x):
    return x * 2 + 1

if __name__ =='__main__':
    alist =  [randint(1,100) for i in range(10)]

    result  = map(func,alist)

    print(list(result))