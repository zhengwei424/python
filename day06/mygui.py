


import tkinter
from functools import partial

root = tkinter.Tk()
lb = tkinter.Label(text='hello world')
b1 = tkinter.Button(root,fg='white',bg='blue',text='OK')
mybtn = partial(tkinter.Button,root,fg='white',bg='blue')
b2 = mybtn(text='xixi')
b3 = mybtn(text='exit',command=root.quit)

lb.pack()
b1.pack()
b2.pack()
b3.pack()
root.mainloop()
