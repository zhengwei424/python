import pickle
from datetime import  datetime

money = [['time', 'income', 'expendture','balance','comment']]


def income(money,balance):
    l = ['',0,0,0,'']
    l[0] = datetime.strftime(datetime.today(),'%Y-%m-%d')
    l[1] = int(input('收入：'))
    l[4] = input('说明：')
    balance += l[1]
    l[3] = balance
    money.append(l)


def expend(money,balance):
    if len(money) > 1 and money[len(money)-1][3] <=0:
        print('没有可用余额')
    l = ['',0,0,0,'']
    l[0] = datetime.strftime(datetime.today(),'%Y-%m-%d')
    l[2] = int(input('支出：'))
    l[4] = input('说明：')
    if l[2] > money[len(money)-1][3]:
        print('余额不足')
    balance -= l[2]
    l[3] = balance
    money.append(l)

def view(money,balance):
    for i in range(len(money)):
        print('%-20s%-20s%-20s%-20s%-20s' % (money[i][0], money[i][1], money[i][2], money[i][3],money[i][4]))
    # print('balance:' % balance)
    input('按Enter继续')

def show_menu(money,balance):
    cmds = {'0': income, '1': expend, '2': view}
    menu = """0--收入
1--支出
2--查询
3--退出
请选择你要进行的操作（0/1/2/3）："""
    while True:
        choice = input(menu).rstrip()[0]
        if choice not in '0123':
            print('Invalid number.Try again')
            continue
        if choice == '3':
            break
        cmds[choice](money,balance)


if __name__ == '__main__':
    show_menu(money,balance=0)
