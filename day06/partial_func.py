from functools import partial

def foo(a,b,c,d,f):
    return a + b +c +d +f

if __name__ == '__main__':
    print(foo(10,20,30,40,5))
    print(foo(10, 20, 30, 40, 6))
    print(foo(10, 20, 30, 40, 7))

    add = partial(foo,*[10,20,30,40])
    # add = partial(foo,a=10,b=20,c=30,d=40)
    print(add(f=5))
    print(add(f=6))
    print(add(f=7))