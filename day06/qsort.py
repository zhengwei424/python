def quick_sort(num_list):
    if len(num_list) < 2:
        return num_list

    middle = num_list[0]
    small=[]
    large=[]
    for i in num_list[1:]:
        if i < middle:
           small.append(i)
        else:
            large.append(i)
    return quick_sort(small) + [middle] + quick_sort(large)


if __name__ == '__main__':
    list = [34,765,64,68,21,32,11,45]
    print(quick_sort(list))

