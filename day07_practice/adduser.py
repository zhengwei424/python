from  subprocess import call
from getpass import getpass


def  username():
    return input('username:')

def  password():
    return getpass('password:')

def useradd_info(username,password):
    data = """useradd_info:
%s:%s
"""
    call('useradd %s' % username,shell=True)
    call('echo %s | passwd --stdin %s' % (password,username),shell=True)
    with open('/tmp/userinfo','w') as fobj:
        fobj.write(data % (username,password))



if __name__ == '__main__':
    useradd_info(username(),password())