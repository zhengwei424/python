from random import randint

def fun1(x):
    return x%2

if __name__ == '__main__':
    l = [randint(1,100) for i in range(10)]
    result = filter(fun1,l)
    print(list(result))
    result2 = filter(lambda x:x%2,l)
    print(list(result2))