import sys
import keyword
import string

first_chs = string.ascii_letters + '_'
all_chs = first_chs + string.digits
def check_id(id):
    if keyword.iskeyword(id):
        print('%s is keyword' % id)
    if id[0] not in first_chs:
        print('1st char is Invalid')
    else:
        for ind in range(len(id)):
            if  id[ind] not in all_chs:
                if ind == 1:
                    print('2nd is invalid,the char is %s' % id[ind])
                elif ind == 2:
                    print('3rd is invalid,the char is %s' % id[ind])
                else:
                    print('%sth is invalid,the char is %s' % (ind+1,id[ind]))
if __name__ == '__main__':
    check_id(sys.argv[1])