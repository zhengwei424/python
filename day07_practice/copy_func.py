import sys

def copy(src_fname,dst_fname):
    with open(src_fname,'rb') as src_fobj:
        with open(dst_fname,'wb') as dst_fobj:
            while True:
                comment = src_fobj.read(4096)
                if not comment:
                    break
                dst_fobj.write(comment)

if __name__ == '__main__':
    copy(sys.argv[1],sys.argv[2])
