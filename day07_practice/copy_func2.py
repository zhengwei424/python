import sys


def copy(src_fname, dst_fname):
    line = []
    with open(src_fname, 'rb') as src_fobj:
        with open(dst_fname, 'wb') as dst_fobj:
             while True:
                str = src_fobj.readline()
                line.append(str)
                if not str:
                    break

             dst_fobj.writelines(line)


if __name__ == '__main__':
    copy(sys.argv[1], sys.argv[2])

