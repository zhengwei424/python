def color(func):
    def red(*args):
        return '\033[31;1m%s\033[0m' % func(*args)
    return red

def hello(word):
    return 'hello %s' % word
@color
def welcome():
    return 'welcome to china'

if __name__ == '__main__':
    hello = color(hello)
    print(hello('beijing'))
    print(welcome())