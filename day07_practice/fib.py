
def fib(x,y,z):
    fib = [x,y]
    if z <= 2:
        fib = [x,y]
    else:
        for i in range(z-2):
            fib.append(fib[-1]+fib[-2])
    return  fib

if __name__ == '__main__':
    print(fib(23,25,10))
