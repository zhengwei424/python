import  sys
def blocks(fobj):
    block = []
    count = 0
    for line in fobj:
        block.append(line)
        count +=1
        if count == 10:
            yield block
            block.clear()
            count=0
    if block:
        yield block
if __name__ == '__main__':
    with open(sys.argv[1]) as fobj:
        for lines in blocks(fobj):
            print(lines)
            print()
        fobj.close()