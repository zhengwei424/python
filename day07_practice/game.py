from random import choice


def computer():
    return choice(['石头', '剪刀', '布'])


def player():
    menu = """三局两胜：
    0--石头
    1--剪刀
    2--布
请出拳（0/1/2）："""
    ch = {'0': '石头', '1': '剪刀', '2': '布'}
    while True:
        choice = input(menu).rstrip()[0]
        if choice not in '012':
            print('Invalid number!')
            continue
        else:
            return ch[choice]


def play_game():
    win = [['石头', '剪刀'], ['剪刀', '布'], ['布', '石头']]
    player_win = 0
    computer_win = 0
    result = []
    while True:
        compare = [computer(), player()]
        if compare[0] != compare[1]:
            if compare in win:
                computer_win += 1
            else:
                player_win += 1
            result.append(compare)
            if computer_win == 2:
                print('you lose')
                print(result)
                break
            elif player_win == 2:
                print('you win')
                print(result)
                break


if __name__ == '__main__':
    play_game()
