import getpass

userdb = {}

def register():
    while True:
        username = input('username:')
        password = getpass.getpass('password:')
        if username == '' or password == '':
            print('username or password cant not be empty.Try again.')
            continue
        elif username in userdb:
            print('%s is exits.' % username)
            continue
        else:
            userdb[username] = password
            break
def login():
    while True:
        username = input('username:')
        password = getpass.getpass('password:')
        if userdb[username] != password or username not in userdb:
            print('Login failed.Try again.')
            continue
        else:
            print('Login successful.')
            break
def show_menu():
    func = {'0':register,'1':login}
    while True:
        menu = """0--register
1--login
2--exit
Please make your choice(0/1/2):"""
        try:
            choice = input(menu).rstrip()[0]
            if choice not in '012':
                continue
            if choice == '2':
                break
            func[choice]()
        except(EOFError, KeyboardInterrupt):
            print()
            return
if __name__ == '__main__':
        show_menu()
