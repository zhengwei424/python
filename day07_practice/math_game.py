from    random import randint,choice

def add(x,y):
    return x+y
def sub(x,y):
    return  x-y

def math_game():
    print('Welcome to funny Math Game')
    count =0
    while True:
        l = [randint(1,100) for i in range(2)]
        l.sort()
        symbol = choice('+-')
        if symbol == '-':
            l.reverse()
            result = sub(l[0],l[1])
        else:
            result = add(l[0],l[1])
        print('%2s%s%3s= ' % (l[0],symbol,l[1]),end='')
        answer = int(input().rstrip())
        if result != answer:
            print('The answer is %s' % result)
            count+=1
            if count ==3:
                print('Game Over')
                break
        else:
            print('Great.Go on!')

if __name__ == '__main__':
    math_game()