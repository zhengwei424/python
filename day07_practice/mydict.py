asict = dict()
dict(['ab','cd'])
bdict = dict([('name','bob'),('age',24)])
for key in bdict:
    print('%s:%s' % (key,bdict[key]))

print('%(name)s:%(age)s' % bdict)

bdict['name'] = 'tom'
bdict['email'] = 'tom@tedu.cn'
print(bdict)
del bdict['email']
print(bdict)
bdict.pop('age')
print(bdict)
bdict.clear()
print(bdict)