from functools import partial

def foo(a,b,c,d):
    return a+b+c+d
if __name__ == '__main__':
    add = partial(foo,a=1,b=2,c=3)
    print(add(d=4))
    print(add(d=5))