myset = set('hello')

len(myset)

for ch in myset:
    print(ch)
aset = set('abc')
print(aset)
bset = set('cde')
print(bset)
print(aset & bset)

print(aset | bset)
print(bset - aset)
aset.add('new')
aset.update(['bbb','ccc'])
print(aset)
aset.remove('ccc')
print(aset)
