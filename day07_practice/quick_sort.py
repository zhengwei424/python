from random import randint

def quick_sort(newlist):
    if len(newlist) < 2:
        return  newlist
    small =[]
    large = []
    middle = newlist[0]
    for i in newlist[1:]:
        if i < middle:
            small.append(i)
        else:
            large.append(i)
    return quick_sort(small) + [middle] + quick_sort(large)
if __name__ == '__main__':
    print(quick_sort([randint(1,100) for i in range(10)]))