

def sum(n):
    '''整数相加'''
    i = 1
    sum = 0
    while i <=n:
        sum +=i
        i +=1
    return sum
def sum_even(n):
    '''偶数相加'''
    i = 1
    sum = 0
    while i <=n:
        if not i%2:
            sum +=i
        i +=1
    return sum

def sum_odd(n):
    '''奇数相加'''
    i = 1
    sum = 0
    while i <= n:
        if i%2:
            sum +=i
        i +=1
    return sum

if __name__ == '__main__':
    print(sum(int(input('Enter the  number :'))))
    print(sum_even(int(input('Enter the  number :'))))
    print(sum_odd(int(input('Enter the  number :'))))
