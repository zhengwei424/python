class Vendor:
    def __init__(self, phone, email):
        self.phone = phone
        self.email = email

    def call(self):
        print('calling %s' % self.phone)


class BearToy:
    def __init__(self, color, size, phone, email):
        self.color = color
        self.size = size
        self.phone = phone
        self.email = email
        self.vendor = Vendor(phone, email)

    def sing(self):
        print('lalala......')

    def speak(self):
        print('My color is %s' % self.color)


class NewBear(BearToy):
    def __init__(self, color, size, phone, email, date):
        super(NewBear,self).__init__(color, size, phone, email)
        self.date = date

    def run(self):
        print('running......')


if __name__ == '__main__':
    tidy = NewBear('Brown', 'middle', '110', 'tedu.cn','2018-07-20')
    print(tidy.color)
    print(tidy.size)
    print(tidy.phone)
    print(tidy.email)
    tidy.vendor.call()
    tidy.run()
    print(tidy.date)
