import hashlib
import sys


def check_md5(fname):
    with open(fname, 'rb') as fobj:
        m = hashlib.md5()  # 创建一个md5对象
        while True:
            data = fobj.read(4096)
            if not data:
                break
            m.update(data)  # 每次读取一部分文件内容，更新至m对象
        return m.hexdigest()


def check(fname1, fname2):
    print(check_md5(fname1))
    print(check_md5(fname2))


if __name__ == '__main__':
    check(sys.argv[1],sys.argv[2])
