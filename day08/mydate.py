class Date:
    def __init__(self,year,month,date):
        self.year = year
        self.month = month
        self.date  = date
    def create(self,dstr):
        y,m,d = map(int,dstr.split('-'))
        dt = Date(y,m,d)
        return dt

if __name__ == '__main__':
    birth_day = Date(1990,4,24)
    print(birth_day.create('2000-5-4'))