class Date:
    def __init__(self, year, month, date):
        self.year = year
        self.month = month
        self.date = date

    @classmethod
    def create(cls, dsrt):
        y, m, d = map(int, dsrt.split('-'))
        dt = cls(y, m, d)
        return dt

    @staticmethod
    def is_date_valid(dstr):
        y, m, d = map(int, dstr.split('-'))
        return 1 <= d <= 31 and 1 <= m <= 12 and y < 4000


if __name__ == '__main__':
    birth_date = Date(1990, 4, 24)
    print(birth_date.create('1990-5-4'))
    print(Date.is_date_valid('2000-6-9'))
    print(birth_date.is_date_valid('3000-8-9'))
