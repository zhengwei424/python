import re
import sys
def count_patt(fname,patt):
    cpatt = re.compile(patt)
    result = {}
    with open(fname) as fobj:
        for line in fobj:
            m = cpatt.search(line)
            if not m:
                result[m] = 1
            else:
                result[m]+=1
    return result
if __name__ == '__main__':
    ip = '^(\d+\.){3}\d+'
    print(count_patt(sys.argv[1],ip))