import socket

host = '192.168.4.254'
port = 12345
addr = (host, port)

c = socket.socket()
c.connect(addr)
while True:
    data = input('> ') + '\r\n'
    c.send(data.encode('utf8'))
    if data.strip() == 'end':
        break
    data = c.recv(1024)
    print(data.strip().decode('utf8'))
c.close()