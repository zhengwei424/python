import socket

from time import strftime

class TcpTimeServer:
    def __init__(self,host='',port=12345):
        self.addr = (host,port)
        self.server = socket.socket()
        self.server.setsockopt(socket.SOL_SOCKET,socket.SO_REUSEADDR,1)
        self.server.bind(self.addr)
        self.server.listen(1)
    def mainloop(self):
        while True:
            cli_sock,cli_addr = self.server.accept()
            self.chat(cli_sock)
        self.server.close()
    def chat(self,cli_sock):
        while True:
            data = cli_sock.recv(1024)
            clock = strftime('%H:%M:%S')
            if data.strip() == b'end':
                break
            data = '[%s]:%s' % (clock,data.decode('utf8'))
            cli_sock.send(data.encode('utf8'))
        cli_sock.close()
if __name__ == '__main__':
    s = TcpTimeServer('',12345)
    s.mainloop()