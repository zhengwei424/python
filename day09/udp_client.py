import socket

host = '192.168.4.254'
port = 12345
addr = (host,port)

c = socket.socket(type=socket.SOCK_DGRAM)

while True:
    data = input('> ') +'\r\n'
    if data.strip() == 'end':
        break
    c.sendto(data.encode('utf8'),addr)
    data = c.recvfrom(1024)
    print(data.decode('utf8'))
    # print(data)
c.close()