import os
import sys

#os.walk
#os.path.join(path,fname)

all_file_path = []

def get_dir(path):
    if os.path.isdir(path):
        if path[-1] == '/':
            get_filename(path)
        elif path[-1] != '/':
            get_filename(path + '/')
    elif os.path.isfile(path):
        # print(path)
        all_file_path.append(path)
    else:
        print('\033[31;1mNo such a file or directory\033[0m')


def get_filename(dir_path):
    for line in os.listdir(dir_path):
        get_dir(dir_path + line)


if __name__ == '__main__':
    try:
        get_dir(sys.argv[1])
    except:
        print('\033[31;1mNo such a file or directory\033[0m')
