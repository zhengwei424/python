import socket
import  time

host = ''
port = 12345
addr = (host,port)
s = socket.socket()
s.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
s.bind(addr)
s.listen(1)


cl_sock,cl_addr = s.accept()
# print('%s is connected' % cl_addr)
while True:
    data = cl_sock.recv(4096)
    clock = time.strftime('%H:%M:%S')
    if data.strip() == b'end':
        break
    data = data.strip()+b'\n'
    data = '[%s] %s' % (clock,data.decode('utf8'))
    cl_sock.send(data.encode('utf8'))