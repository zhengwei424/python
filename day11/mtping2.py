import threading
from random import randint
import subprocess


class Ping:
    def __init__(self,host):
        self.host =  host


    def __call__(self, *args, **kwargs):
        rc = subprocess.call(
            'ping -c 2 %s &>/dev/null' % self.host,shell=True
        )
        if rc:
            print('%s down'%self.host)
        else:
            print('%s up'%self.host)
if __name__ == '__main__':

    ip = ('172.40.58.%s' % i for i in range(1,255))
    for item in ip:
        t = threading.Thread(target=Ping(item))
        t.start()