import os

print('Starting......')

pid = os.fork()

if pid :
    print('In parent')
else:
    print('In child')
print('Done')