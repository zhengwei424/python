import subprocess
import os
import threading

def ping(host):
    rc = subprocess.call('ping -c 2 %s &> /dev/null' % host,shell=True)
    if rc:
        print('%s is down' % host)
    else:
        print('%s is up' % host)
if __name__ == '__main__':
    for i in range(1,255):
        ip = '172.40.58.%s' % i
        # pid = os.fork()
        # if not pid:
        #     ping(ip)
        #     exit()

        t = threading.Thread(target=ping,args=(ip,))
        t.start()