import os
import time

pid = os.fork()

if pid:
    print('parent is sleepping......')
    time.sleep(60)
    print('parent done')

else:
    print('child is sleepping......')
    time.sleep(10)
    print('child done')
