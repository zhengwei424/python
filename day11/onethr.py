from  time import sleep,ctime
import threading


def loop0():
    print('start loop0 at:',ctime())
    sleep(4)
    print('loop0 done at:',ctime())

def loop1():
    print('start loop1 at:',ctime())
    sleep(2)
    print('loop1 done at:',ctime())

def main():
    print('starting at:',ctime())
    threading._start_new_thread(loop0,())
    threading._start_new_thread(loop1,())
    sleep(10)
    print('all done at:',ctime())
if __name__ == '__main__':
    main()