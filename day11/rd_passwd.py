import string
from random import choice

all_choices = string.ascii_letters + string.digits
result = [choice(all_choices) for i in range(8)]
print(''.join(result))