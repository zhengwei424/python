import os
import socket
import time


class tcp_server:
    def __init__(self,host,port,listen_num,cli_sock,cli_addr):
        self.host = host
        self.port = port
        self.listen_num = listen_num
        self.cli_sock = cli_sock
        self.cli_addr = cli_addr
        self.addr = (self.host,self.port)
        self.server = socket.socket()
        self.server.setsockopt(socket.SOL_SOCKET,socket.SO_REUSEADDR,1)
        self.server.bind(self.addr)
        self.server.listen(listen_num)
    def chat(self):
        while True:
            data = self.cli_sock.recv(4096)
            clock = time.strftime('%H:%M:%S')
            if data.strip() == b'end':
                break
            data = data.strip()+b'\r\n'
            data = '[%s] %s' % (clock,data.decode('utf8'))
            self.cli_sock.send(data.encode('utf8'))
        self.cli_sock.close()
    def mainloop(self):
        while True:
            self.cli_sock,self.cli_addr = self.server.accept()
            self.chat()
        self.server.close()

if __name__ == '__main__':
    s = tcp_server('',12345,1,'','')
    s.mainloop()
