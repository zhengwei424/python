import sys


class Convert:
    def __init__(self, option, src_fname, dst_fname):
        self.option = option
        self.src_fname = src_fname
        self.dst_fname = dst_fname

    def __str__(self):
        return self.option

    def to_windows(self):
        with open(self.src_fname, 'rb') as src_fobj:
            with open(self.dst_fname, 'wb') as dst_fobj:
                for line in src_fobj:
                    line = line.rstrip() + b'\r\n'
                    dst_fobj.write(line)

    def to_linux(self):
        with open(self.src_fname, 'rb') as src_fobj:
            with open(self.dst_fname, 'wb') as dst_fobj:
                for line in src_fobj:
                    line = line.rstrip() + b'\n'
                    dst_fobj.write(line)


if __name__ == '__main__':
    try:
        convert = Convert(sys.argv[1], sys.argv[2], sys.argv[3])
        s = str(convert)
        if s == '-w':
            convert.to_windows()
        elif s == '-l':
            convert.to_linux()
    except(IndexError):
        print('Ivalid options or Missing parameters,Try again.the options are "-w" or "-l"')
    else:
        print('Ivalid options,Try again.the options are "-w" or "-l"')
