# 题目：有四个数字：1、2、3、4，能组成多少个互不相同且无重复数字的三位数？各是多少？
# 程序分析：可填在百位、十位、个位的数字都是1、2、3、4。组成所有的排列后再去 掉不满足条件的排列。

from  random import choice
def num():
    str = '1234'
    l = []
    ch = ''
    for i in range(4):
        for j in range(4):
            for m in range(4):
                if i != j and i != m and j != m:   #不能写成 i ！=  j！= m
                    ch = str[i]+str[j]+str[m]
                    l.append(ch)
    print(l)
    return len(l)

if __name__ == '__main__':
    result =num()
    print('数字个数为：%s' % result)




#拓展：0～9组成n位数字