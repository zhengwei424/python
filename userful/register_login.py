import getpass

userdb = {'xixi':'lele'}

def register():
    username = input('username:')
    if username in userdb:
        print('%s already exits.' % username)
    else:
        password = input('password:')
        userdb[username] = password
def login():
    username = input('username:')
    password = getpass.getpass('password:')  #getpass.getpass()自带input功能

    if userdb.get(username) != password:
        print('\033[31;1mlogin failed\033[0m')
    else:
        print('\033[32;1mlogin successful\033[0m')

def show_menu():
    cmds = {'0':register,'1':login}
    prompt = """0--register
1--login
2--exit
Please make your choice(0/1/2):"""
    while True:
        choice = input(prompt).strip()[0]
        if choice not in '012':
            print('Invalid input.Try again.')
            continue
        if choice == '2':
            break
        cmds[choice]()
if __name__ == '__main__':
    show_menu()
