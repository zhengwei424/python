#用于比较今天的网页访问 与 昨天有什么不同，并记录
#知识点：集合


with open('pass') as fobj:
    aset = set(fobj)

with open('mima') as fobj:
    bset = set(fobj)

with open('diff.txt','w') as fobj:
    fobj.writelines(bset - aset)