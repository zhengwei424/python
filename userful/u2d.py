
#跨平台文件格式转换


import sys

def unix2dos(fname):
    dst_fname = fname + '.txt'

    with open(fname) as src_fobj:
        with open(dst_fname,'w') as dst_fobj:
            for line in src_fobj:
                line = line.rstrip() + '\r\n'  #windows系统文件每一行使用\r\n结尾  linux使用\n结尾
                dst_fobj.write(line)

if __name__ == '__main__':
    unix2dos(sys.argv[1])